public class BSTNode<T> {

    public T data;
    public BSTNode left;
    public  BSTNode right;
    public BSTNode(T value){
        this.data = value;
    }

}
